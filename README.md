# Python Practice Project - Report Generator (Company Departments)

## Getting setup

1. Fork and clone this starter project
2. Change (`cd`) into the newly cloned directory
3. Create a new virtual environment in the repository directory for the project
4. Activate the virtual environment
5. Use `pip install -r requirements.txt` to install the necessary dependencies

## Employee Data

The `employee_report.py` file already has a bit of code. It imports a module from the resources directory and uses a method to randomly generate employee data. The call to this method has already been implemented and assigned to a variable called `employee_list`. **This list contains your data** - it is a *list of dictionaries*. Each dictionary represents an employee. Here is an example:
```
[
    {
        'first_name': 'Michael',
        'last_name': 'Hernandez',
        'id': 4888576,
        'department': 'Maintenance',
        'is_manager': False,
        'salary': 102000,
        'hire_date': '2021-11-23'
    }
    {
        'first_name': 'Daniel',
        'last_name': 'Rubio',
        'id': 9110076,
        'department': 'HR',
        'is_manager': True,
        'salary': 126000,
        'hire_date': '2017-02-17'
    }
]
```

## Project Requirements

In the `employee_report.py` file write the code necessary to print to the screen a summary of employee data organized by department. For each department it should include:
- The total number of employees in that department
- The average salary for that department
- The names and id's of the employees who are managers
- The names and salaries of the employees who earn 150,000 or more
- *Extra credit* = The names and hire dates of the employees who have been employed for more than 20 years
- NOTE: Any of these items which match no employees should display "No employees"

For example:
```
########## Accounting ##########
Number of employees: 37
Average salary: 89000

Managers:
- Bender Rodriguez (648376503)

Top earners $150000+:
- Louise Belcher ($177000)
- Stewie Griffin ($151000)

Senior personnel 20+ years:
- Bob Belcher (hired: 1995-11-04)
- Stewie Griffin (hired: 2002-12-16)
- Marge Simpson (hired: 1998-03-08)


########## Maintenance ##########
Number of employees: 19
Average salary: 75000

Managers:
No employees
...
...
```

### NOTE: You can implement your report generator however you like, however it must contain:
- At least 1 class
- At least 1 function or class method (you'll probably need more)
- Another module in the resources directory containing one or more of the classes and/or functions mentioned above. You'll import and use this module in your `employee_report.py` file