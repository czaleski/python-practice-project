"""
This file is used to randomly generate employee data.
NOTE: Please do not make any changes to this file until
afer you have completed the original assignment. After
that, feel free to play.
"""

from faker import Faker
from enum import Enum

class Department(Enum):
    SALES = "Sales"
    MARKETING = "Marketing"
    IT = "IT"
    ACCOUNTING = "Accounting"
    HR = "HR"
    ADMINISTRATION = "Administration"
    FINANCE = "Finance"
    MAINTENANCE = "Maintenance"

def generate_employees(num_employees):
    fake = Faker()
    employees = []
    for _ in range(num_employees):
        new_employee = {
            "first_name": fake.first_name(),
            "last_name": fake.last_name(),
            "id": fake.pyint(1000000, 9999999),
            "department": fake.enum(Department).value,
            "is_manager": fake.boolean(chance_of_getting_true=20),
            "salary": fake.pyint(10, 175) * 1000,
            "hire_date": fake.past_date("-25y").strftime("%Y-%m-%d"),
        }
        employees.append(new_employee)
    return employees
